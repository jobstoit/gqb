// Copyright 2019 Job Stoit. All rights reserved.

package main

import (
	"strings"
)

// DataType is a models data structure
type DataType interface {
	SQL() string
}

// Model is the full db model structure and configuration
// The model contains all the nessasary information for
// Creating Query builders
type Model struct {
	Driver string
	Pkg    string
	Tables []*Table
	Types  []DataType
}

// Table is a definition of a database table
type Table string

// String is the stringer implementation
func (x Table) String() string {
	return string(x)
}

// FindColumns returns the columns with a reference to this specific table
func (x *Table) FindColumns(types []DataType) (c []*Column) {
	for _, typ := range types {
		if col, ok := typ.(*Column); ok && col.Table == x {
			c = append(c, col)
		}
	}
	return
}

// Column contains the table column properties and
// lies at the core of the query builder.
// Note that if the datatype is of type column that
// the column is a foreign key.
type Column struct {
	Table       *Table
	Name        string
	DataType    DataType
	Size        int
	Default     string
	Nullable    bool
	Unique      bool
	Primary     bool
	Constraints []string
	rawType     string
}

// SQL is the datatype implementation
func (x Column) SQL() string {
	return x.DataType.SQL()
}

func (x Column) String() string {
	return string(*x.Table) + `.` + x.Name
}

// Enum is en e nummeric object which can be
// defined as type in the database
type Enum struct {
	Table  *Table
	Values []string
}

// SQL is the DataType implementation
func (x *Enum) SQL() string {
	return strings.Join(x.Values, `, `)
}

// PrimativeType is a primative database type
type PrimativeType string

// SQL is the datatype implementation
func (x PrimativeType) SQL() string {
	return string(x)
}

// GetType returns the full type of the column
func (x Model) GetType(c *Column) {
	if p := getPrimativeType(c.rawType); p != nil {
		c.DataType = p
		return
	}

	for _, typ := range x.Types {
		if col, ok := typ.(*Column); ok && col != nil &&
			(c.rawType == col.Table.String()+`.`+col.Name ||
				c.rawType == col.Table.String()+`(`+col.Name+`)`) {
			c.DataType = col
			return
		}

		if enum, ok := typ.(*Enum); ok && enum != nil &&
			c.rawType == string(*enum.Table) {
			c.DataType = enum
			return
		}
	}
	//panic(`Type not found`)
}

// getPrimativeType retuns the primative type matching the
// given query
func getPrimativeType(i string) DataType {
	switch i {
	case `string`, `char`, `character`, `charactering varying`:
		return getPrimativeType(`varchar`)

	case `integer`:
		return getPrimativeType(`int`)

	case `real`:
		return getPrimativeType(`float`)

	case `time`, `timestamp`:
		return getPrimativeType(`datetime`)

	case `bool`:
		return getPrimativeType(`boolean`)

	case `varchar`, `text`, `int`, `tinyint`, `smallint`, `bigint`, `double`, `float`, `date`, `datetime`, `boolean`:
		return PrimativeType(i)
	default:
		return nil
	}
}
