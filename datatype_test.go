// Copyright 2019 Job Stoit. All rights reserved.

package main

import "testing"

var (
	testTableUser  = Table(`user`)
	testTableImage = Table(`image`)
	testTableRole  = Table(`role`)

	testModel = Model{
		Tables: []*Table{&testTableUser, &testTableImage, &testTableRole},
		Types: []DataType{
			&Column{
				Table:   &testTableUser,
				Name:    `id`,
				Primary: true,
				rawType: `int`,
			},
			&Column{
				Table:   &testTableUser,
				Name:    `name`,
				rawType: `varchar`,
			},
			&Column{
				Table:   &testTableUser,
				Name:    `role`,
				rawType: `role`,
			},
			&Column{
				Table:   &testTableImage,
				Name:    `id`,
				Primary: true,
				rawType: `int`,
			},
			&Column{
				Table:   &testTableImage,
				Name:    `user_id`,
				rawType: `user.id`,
			},
			&Enum{
				Table:  &testTableRole,
				Values: []string{`ADMIN`, `GENERAL`},
			},
		},
	}
)

func TestGetPrimativeType(t *testing.T) {
	expectStr(t, `int`, getPrimativeType(`integer`).SQL())
	expectStr(t, `varchar`, getPrimativeType(`char`).SQL())
	expectStr(t, `float`, getPrimativeType(`real`).SQL())
	expectStr(t, `boolean`, getPrimativeType(`bool`).SQL())
	expectStr(t, `datetime`, getPrimativeType(`time`).SQL())
	expectStr(t, `smallint`, getPrimativeType(`smallint`).SQL())
}

func TestGetType(t *testing.T) {
	for _, typ := range testModel.Types {
		if col, ok := typ.(*Column); ok {
			testModel.GetType(col)
			if col.Name == `user_id` && col.SQL() != `int` {
				t.Error(`user_id is not of the expected type`)
			}
			if col.Name == `role` && col.SQL() != `ADMIN, GENERAL` {
				t.Error(`role does not refer to role enum`)
			}
		}
	}

}

func TestFindTable(t *testing.T) {
	amm := 0
	for _, i := range testTableUser.FindColumns(testModel.Types) {
		switch i.String() {
		case `user.id`, `user.name`, `user.role`:
			amm++
		default:
			break
		}
	}
	expectInt(t, 3, amm)
}
