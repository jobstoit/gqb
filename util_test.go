// Copyright 2019 Job Stoit. All rights reserved.

package main

import "testing"

func expectInt(t *testing.T, expected, actual int) {
	if expected != actual {
		t.Errorf("TEST FAILED\nExpected: %d\nActual: %d\n", expected, actual)
	}
}

func expectStr(t *testing.T, expected, actual string) {
	if expected != actual {
		t.Errorf("TEST FAILED\nExpected: %s\nActual: %s\n", expected, actual)
	}
}
